class EmployeesController < ApplicationController
  before_action :set_employee, only: [:show, :edit, :update, :destroy]

  # GET /employees
  # GET /employees.json
  def index
    @employees = current_user.employees.where(["first_name LIKE ?","%#{params[:search]}%"])
  end

  # GET /employees/1
  # GET /employees/1.json
  def show
    @department = Department.find(@employee.department_id)
  end

  # GET /employees/new
  def new
    @department = Department.find(@employee.department_id)
    @employee = Employee.new
  end

  # GET /employees/1/edit
  def edit
    @department = Department.find(@employee.department_id)
  end

  # POST /employees
  # POST /employees.json
  def create
    @employee = Employee.new(employee_params)
    respond_to do |format|
      if @employee.save
        format.html { redirect_to Department.find(@employee.department_id), notice: 'Employee was successfully created.' }
        format.json { render :show, status: :created, location: @employee }
      else
        format.html { render :new, notice: 'Employee was not successfully created.' }
        format.json { render json: @employee.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /employees/1
  # PATCH/PUT /employees/1.json
  def update
    respond_to do |format|
      if @employee.update(employee_params)
        format.html { redirect_to Department.find(@employee.department_id), notice: 'Employee was successfully updated.' }
        format.json { render :show, status: :ok, location: @employee }
      else
        format.html { render :edit }
        format.json { render json: @employee.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /employees/1
  # DELETE /employees/1.json
  def destroy
    @department = Department.find(@employee.department_id)
    @employee.destroy
    respond_to do |format|
      format.html { redirect_to @department, notice: 'Employee was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_employee
      @employee = Employee.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def employee_params
      params.require(:employee).permit(:first_name, :last_name, :ci, :phone_number, :email, :bank, :account_type, :count_number, :landline, :emergency_phone, :department_id)
    end
end
