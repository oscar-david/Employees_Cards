json.extract! employee, :id, :first_name, :last_name, :ci, :phone_number, :email, :bank, :account_type, :count_number, :landline, :emergency_phone, :department_id, :created_at, :updated_at
json.url employee_url(employee, format: :json)
